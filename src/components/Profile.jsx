'use client'
import { useState } from "react";
import Modal from "./Modal";
import PencilIcon from "./PencilIcon";

export default function Profile() {
  const [firstName, setFirstName] = useState("Reka");
  const [lastName, setLastName] = useState("Darko");
  const [modalOpen, setModalOpen] = useState(false);
  const [avatarUrl, setAvatarUrl] = useState(
    "../../tengrai.jpeg"
  );

  const updateAvatar = (imgSrc) => {
    setAvatarUrl(imgSrc);
    setModalOpen(false);

  }
  const bioText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
  Phasellus ullamcorper, elit eget posuere mollis, risus velit porttitor ligula, 
  id tincidunt enim odio ac justo. Etiam euismod, turpis vel vestibulum tempus, 
  elit turpis aliquam nisi, id pulvinar eros turpis quis libero.`;

const studiesText = `Sed sed felis sit amet nisi volutpat sollicitudin eget ac sapien. 
  Fusce pretium justo at posuere congue. `;

  const hobbiesText = `Sed sed felis sit amet nisi volutpat sollicitudin eget ac sapien. 
  Fusce pretium justo at posuere congue. Etiam euismod, turpis vel vestibulum tempus, 
  elit turpis aliquam nisi, id pulvinar eros turpis quis libero. `;

  const aboutMeText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
  Phasellus ullamcorper, elit eget posuere mollis, risus velit porttitor ligula, 
  id tincidunt enim odio ac justo. Etiam euismod, turpis vel vestibulum tempus, 
  elit turpis aliquam nisi, id pulvinar eros turpis quis libero. Sed sed felis sit amet nisi volutpat sollicitudin eget ac sapien. 
  Fusce pretium justo at posuere congue.`;
  

  return (
    <div className="flex flex-col items-center pt-12">
      <div className="relative">
        <img
          src={avatarUrl}
          alt="Avatar"
          className="w-[150px] h-[150px] rounded-full border-2 border-gray-400"
        />
        <button
          className="absolute -bottom-3 left-0 right-0 m-auto w-fit p-[.35rem] rounded-full bg-gray-800 hover:bg-gray-700 border border-gray-600"
          title="Change photo"
          onClick={() => setModalOpen(true)}
        >
          <PencilIcon />
        </button>
      </div>
      <h2 className="text-white font-bold mt-6">
        {firstName} {lastName}
      </h2>
      <p className="text-gray-500 text-xs mt-2">Student</p>
      {modalOpen && (
        <Modal
          updateAvatar={updateAvatar}
          closeModal={() => setModalOpen(false)}
        />
      )}

{/* BUTTON */}

{/* BIO AND STUDIES */}
<div className="w-3/4 md:w-2/5 py-2 my-2">
        <div className="bg-gray-600 hover:bg-gray-900 rounded-lg p-4 mt-4">
          <h3 className="text-white text-lg font-semibold mb-2">BIO</h3>
          <p className="text-white">{bioText}</p>
        </div>
        <div className="bg-gray-800 hover:bg-gray-900 rounded-lg p-4 mt-4">
          <h3 className="text-white text-lg font-semibold mb-2">Studies</h3>
          <p className="text-white">{studiesText}</p>
        </div>
        <div className="bg-gray-600 hover:bg-gray-900 rounded-lg p-4 mt-4">
          <h3 className="text-white text-lg font-semibold mb-2">Hobbies</h3>
          <p className="text-white">{hobbiesText}</p>
        </div>
        <div className="bg-gray-800 hover:bg-gray-900 rounded-lg p-4 mt-4">
          <h3 className="text-white text-lg font-semibold mb-2">About me</h3>
          <p className="text-white">{aboutMeText}</p>
        </div>
      </div>

      <div className="my-6"></div>
    </div>
  );
}
